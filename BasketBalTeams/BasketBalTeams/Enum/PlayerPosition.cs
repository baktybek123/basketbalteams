﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BasketBalTeams.Enum
{
    public enum PlayerPosition
    {
        PG =1,
        SG ,
        SF,
        PF,
        C
    }
}
