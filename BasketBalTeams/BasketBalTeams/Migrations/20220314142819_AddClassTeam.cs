﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace BasketBalTeams.Migrations
{
    public partial class AddClassTeam : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "TeamId",
                table: "Persons",
                type: "int",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.CreateTable(
                name: "Teams",
                columns: table => new
                {
                    ID = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<string>(type: "nvarchar(450)", nullable: true),
                    Country = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Conference = table.Column<int>(type: "int", nullable: false),
                    YearOfCreation = table.Column<DateTime>(type: "datetime2", nullable: false),
                    NumberOfWinsInASeason = table.Column<int>(type: "int", nullable: false),
                    NumberOfLossInASeason = table.Column<int>(type: "int", nullable: false),
                    TeamTrainer = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Teams", x => x.ID);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Persons_JerseytNumber",
                table: "Persons",
                column: "JerseytNumber",
                unique: true,
                filter: "[JerseytNumber] IS NOT NULL");

            migrationBuilder.CreateIndex(
                name: "IX_Persons_TeamId",
                table: "Persons",
                column: "TeamId");

            migrationBuilder.CreateIndex(
                name: "IX_Persons_TeamId1",
                table: "Persons",
                column: "TeamId",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_Teams_Name",
                table: "Teams",
                column: "Name",
                unique: true,
                filter: "[Name] IS NOT NULL");

            migrationBuilder.AddForeignKey(
                name: "FK_Persons_Teams_TeamId",
                table: "Persons",
                column: "TeamId",
                principalTable: "Teams",
                principalColumn: "ID",
                onDelete: ReferentialAction.NoAction);

            migrationBuilder.AddForeignKey(
                name: "FK_Persons_Teams_TeamId1",
                table: "Persons",
                column: "TeamId",
                principalTable: "Teams",
                principalColumn: "ID",
                onDelete: ReferentialAction.NoAction);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Persons_Teams_TeamId",
                table: "Persons");

            migrationBuilder.DropForeignKey(
                name: "FK_Persons_Teams_TeamId1",
                table: "Persons");

            migrationBuilder.DropTable(
                name: "Teams");

            migrationBuilder.DropIndex(
                name: "IX_Persons_JerseytNumber",
                table: "Persons");

            migrationBuilder.DropIndex(
                name: "IX_Persons_TeamId",
                table: "Persons");

            migrationBuilder.DropIndex(
                name: "IX_Persons_TeamId1",
                table: "Persons");

            migrationBuilder.DropColumn(
                name: "TeamId",
                table: "Persons");
        }
    }
}
