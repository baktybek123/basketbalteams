﻿using BasketBalTeams.Interface;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BasketBalTeams.Models
{
    public abstract class Person : IEntitys<int>
    {
        public int ID { get; set; }
        [ForeignKey("Team")]
        public int TeamId { get; set; }
        [Required]
        public string FirstName { get; set; }
        [Required]
        public string SurName { get; set; }
        public DateTime DateOfBirth { get; set; }

        public virtual Team Team { get; set; }
    }
}
