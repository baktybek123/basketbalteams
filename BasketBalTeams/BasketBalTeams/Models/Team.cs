﻿using BasketBalTeams.Enum;
using BasketBalTeams.Interface;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BasketBalTeams.Models
{
    public class Team : IEntitys<int>
    {
        public int ID { get; set; }
        public string Name { get; set; }
        public string Country { get; set; }
        public Conference Conference { get; set; }
        public DateTime YearOfCreation { get; set; }
        public int NumberOfWinsInASeason { get; set; }
        public int NumberOfLossInASeason { get; set; }        
        
        public virtual Trainer Trainer { get; set; }

        public virtual ICollection<Player> Players { get; set; }

        

    }
}
