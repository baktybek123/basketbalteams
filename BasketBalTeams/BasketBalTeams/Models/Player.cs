﻿using BasketBalTeams.Enum;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BasketBalTeams.Models
{
   public class Player : Person 
    {
       
        public int Growth { get; set; }
        public int Weight { get; set; }
        [Required]
        public int JerseytNumber { get; set; }
        [Required]
        public PlayerPosition PlayerPosition { get; set; }        
        
       
    }
}
