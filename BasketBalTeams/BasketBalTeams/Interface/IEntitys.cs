﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BasketBalTeams.Interface
{
    public interface IEntitys<T>
    {
        public T ID { get; set; }
    }
}
